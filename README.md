# Script for search common friends

## What does this script do?

Imagine you have person whit private profile and you want to know who is his friends. You can use this script to find common friends between you and this person.

## What you need?

You must have a valid Vkontakte account, a person with a closed profile for whom you need to identify friends and a person who may have mutual friends with the person you are looking for

## How to install and run?

You must have installed [python3](https://www.python.org/downloads/) and [redis-server](https://redis.io/download)

1. Clone this repository `git clone https://gitlab.com/bibbob/vk-common-friends.git`
2. Install requirements `pip install -r requirements.txt`
3. Run redis-server `redis-server ./redis.conf`
4. Run script `python3 main.py`

So, this is full version
```bash
git clone https://gitlab.com/bibbob/vk-common-friends.git
cd vk-common-friends
pip install -r requirements.txt
redis-server ./redis.conf
python3 main.py
```