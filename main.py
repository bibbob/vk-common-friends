import vk_api
from redis import Redis

redis: None
vk: None
search_id: None


def auth_handler():
    return input('Enter 2fa code: '), True


def is_user_exists(user_id):
    return redis.exists(user_id)


def get_friends(user_id):
    try:
        if not is_user_exists(user_id):
            redis.sadd(user_id, *vk.friends.get(user_id=user_id)['items'])
        return redis.smembers(user_id)
    except:
        return []


def common_friends(user_id):
    friends = get_friends(user_id)
    return len(list(filter(lambda x: x == search_id, friends))) != 0


def init():
    global redis, vk

    redis = Redis(port=9876, decode_responses=True, encoding='utf-8')
    vk_session = vk_api.VkApi(
        input('Enter your phone: '),
        input('Enter your password: '),
        auth_handler=auth_handler
    )
    vk_session.auth()

    vk = vk_session.get_api()


def main():
    init()
    
    global search_id
    search_id = input('Enter id of user to search: ')

    can_have_common_friends = input('Enter id of user, who can have common friends: ')

    print('starting...')

    for i in get_friends(can_have_common_friends):
        if common_friends(i):
            print('Found good id:', i)
            redis.sadd('good', i)


if __name__ == '__main__':
    main()
